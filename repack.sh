#!/bin/sh
if [ $2 == "voodoo" ]; then
  echo "imoseyon_kernel_gtab_gb_$1voodoo" > initramfs_root/kernel_version
else 
  echo "imoseyon_kernel_gtab_gb_$1" > initramfs_root/kernel_version
fi
/data/utils/kernel_repack_utils/repacker.sh -s zImage -d /tmp/new_zImage -r initramfs_root -c gzip
cd zip
cp /tmp/new_zImage kernel_update/zImage
rm *.zip
zip -r imoseyon_kernel_gtab_gb_$1voodoo.zip *
chown -R imoseyon *
cp *.zip /tmp
